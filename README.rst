=============================
certificate-trust dib element
=============================

This elemets adds a certificate to the system trust.

----------
Parameters
----------

:DIB_CA_CERTIFICATE_PEM: The certificate (PEM) to be added. Required. 
:DIB_CA_CERTIFICATE_NAME: The filename given to the certificate on the
  image. Optional, defaults to certificate.pem.
